﻿using DemoWebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoWebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class StudentController : ControllerBase
	{
		//create student list
		static List<Student> data = new List<Student>()
		{
			new Student("1", "Nguyen Duc Anh", 21),
			new Student("2", "Nguyen Van C", 30),
			new Student("3", "Khanh an cut", 20),
			new Student("4", "Mixu", 22),
			new Student("5", "Pham Hai Yen Anh", 30),
			new Student("6", "an cut", 20)
		};
		//create api/getAll: get all students
		[HttpGet]
		public IActionResult GetAllStudent()
		{
			return Ok(data);
		}

		//create apt/get/1: get student by id, if not have result => show: No data

		[HttpGet("{id}")]
		public IActionResult GetStudentById(string id)
		{
			var s = data.Where(s=>s.Id.Contains(id)).ToList();
			if (String.IsNullOrEmpty(id))
			{
				return BadRequest("id field is required.");
			}
			if (s.Count == 0)
			{
				return NotFound("NO data");
			}
			else
			{
				return Ok(s);
			}
		}

		//create api/post: add stduent to data
		//check empty, code, exist, age > 18...
		[HttpPost]
		public IActionResult AddStudent([FromBody] Student student)
		{
			if (student == null)
			{
				return BadRequest("Student data is required.");
			}

			var existingStudent = data.FirstOrDefault(s => s.Id == student.Id);
			if (existingStudent != null)
			{
				return Conflict("Id already exists.");
			}

			//if (string.IsNullOrEmpty(student.Id) || string.IsNullOrEmpty(student.Name) || student.Age <= 0)
			//{
			//	return BadRequest("Invalid student data. Make sure id, name, and age are provided and age is greater than 0.");
			//}

			if (student.Age < 18)
			{
				return BadRequest("Age must be greater than 18.");
			}

			data.Add(student);
			return Ok(data);
		}


		//create api/put: update student in data
		// check empty, age > 18
		[HttpPut]
		public IActionResult Update([FromBody] Student student)
		{
			if (student == null)
			{
				return BadRequest("Please input student");
			}
			var s = data.FirstOrDefault(z => z.Id == student.Id);
			if (s == null)
			{
				return BadRequest("Id not exist");
			}
			else
			{
				s.Name = student.Name;
				s.Age = student.Age;
			}
			return Ok(data);
		}
		

		//create api/delete: delete student in data by id
		//check if id exist: "Id not exist"
		[HttpDelete]
		public IActionResult Delete([FromBody] Student student)
		{
			if (student == null)
			{
				return BadRequest("Please input student");
			}
			var s = data.FirstOrDefault(z => z.Id == student.Id);
			if (s == null)
			{
				return BadRequest("Id not exist");
			}
			else
			{
				data.Remove(s);
			}
			return Ok(data);
		}
		
	}
}
