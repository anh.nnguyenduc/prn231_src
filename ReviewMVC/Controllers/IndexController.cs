﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReviewMVC.Models;
using System.Linq;

namespace ReviewMVC.Controllers
{
	public class IndexController : Controller
	{
		public IActionResult Index(int categoryFilter)
		{
			MySaleDBContext con = new MySaleDBContext();
			var cate = con.Categories.ToList();
			ViewBag.cate = cate.ToList();
			var cateFilter = con.Categories.ToList();
			cateFilter.Insert(0, new Category { CategoryId = 0, CategoryName = "All" });
			ViewBag.cateFilter = cateFilter;
			IQueryable<Product> products = con.Products.Include(x => x.Category);
			if (categoryFilter != 0)
			{
				products = products.Where(p => p.CategoryId == categoryFilter);
			}
			ViewBag.list = products.ToList();
			return View();
		}

		public IActionResult GetProductDetail(int productId)
		{
			MySaleDBContext con = new MySaleDBContext();
			var product = con.Products.Include(x => x.Category).FirstOrDefault(x => x.ProductId == productId);
			if (product == null)
			{
				return Json(new { success = false });
			}
			return Json(new
			{
				productId = product.ProductId,
				productName = product.ProductName,
				unitPrice = product.UnitPrice,
				unitsInStock = product.UnitsInStock,
				image = product.Image,
				categoryId = product.CategoryId
			});
		}

		[HttpPost]
		public IActionResult DeleteProduct(int productId)
		{
			MySaleDBContext con = new MySaleDBContext();
			var product = con.Products.FirstOrDefault(x => x.ProductId == productId);
			if (product != null)
			{
				con.Products.Remove(product);
				con.SaveChanges();
				return Ok();
			}
			return NotFound();
		}


		[HttpPost]
		public IActionResult UpdateProduct(Product updatedProduct)
		{
			MySaleDBContext con = new MySaleDBContext();
			var product = con.Products.FirstOrDefault(x => x.ProductId == updatedProduct.ProductId);
			if (product != null)
			{
				product.ProductName = updatedProduct.ProductName;
				product.UnitPrice = updatedProduct.UnitPrice;
				product.UnitsInStock = updatedProduct.UnitsInStock;
				product.Image = updatedProduct.Image;
				product.CategoryId = updatedProduct.CategoryId;

				con.SaveChanges();
				return Ok();
			}
			return NotFound();
		}


		[HttpPost]
		public IActionResult AddProduct(IFormCollection f)
		{
			MySaleDBContext con = new MySaleDBContext();
			var product = con.Products.FirstOrDefault(x => x.ProductId == int.Parse(f["productId"]));
			if (product == null)
			{
				string name = f["productName"];
				decimal price = decimal.Parse(f["productPrice"]);
				int stock = int.Parse(f["productStock"]);
				string img = f["productImage"];
				int cateId = int.Parse(f["categorySelect"]);

				Product p = new Product()
				{
					ProductName = name,
					UnitPrice = price,
					UnitsInStock = stock,
					Image = img,
					CategoryId = cateId
				};
				con.Products.Add(p);
				con.SaveChanges();
				return Ok();
			}
			return NotFound();
		}
	}
}
