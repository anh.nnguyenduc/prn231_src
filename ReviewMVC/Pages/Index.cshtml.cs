﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ReviewMVC.Models;

namespace ReviewMVC.Pages
{
    public class IndexModel : PageModel
    {
        private readonly MySaleDBContext _context;

        public IndexModel(MySaleDBContext context)
        {
            _context = context;
        }

        public void OnGet()
        {

        }
    }
}